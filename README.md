# Angular Basics 
### Okt/Nov 2020

Welcome to this course that will get you up to speed with web app development using Angular.

# Links

* [Installation Instructions](./installation-instructions.md)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* [Course Details](https://www.ribomation.se/courses/web/angular-basics)

# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone this repo. 

    mkdir -p ~/angular-course/my-solutions
    cd ~/angular-course
    git clone <https url to this repo> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/angular-course/gitlab
    git pull

# Build Solution/Demo Programs
First, copy the content of an app directory to a fresh folder, outside the gitlab folder.
Second, open a terminal window in the new directory and install all
dependencies with the command below.

    npm install


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
