import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {WelcomeComponent} from './pages/welcome/welcome.component';
import {AboutComponent} from './pages/about/about.component';
import {UsersComponent} from './pages/users/users.component';
import {HttpClientModule} from '@angular/common/http';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {RoutingModule} from './routing.module';


@NgModule({
    declarations: [
        AppComponent,
        WelcomeComponent,
        AboutComponent,
        UsersComponent,
        NotFoundComponent
    ],
    imports: [
        BrowserModule,
        // RouterModule.forRoot(routingTable),
        HttpClientModule,
        RoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
