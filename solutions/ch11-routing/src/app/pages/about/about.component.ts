import {Component} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styles: ['']
})
export class AboutComponent {
    constructor(private titleSvc: Title) {
        titleSvc.setTitle('About')
    }
}
