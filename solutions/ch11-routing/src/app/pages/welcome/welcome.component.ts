import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';


@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent {
  greeting = 'Hi there !!!';
  image = 'https://picsum.photos/id/0/800/400';

  constructor(private titleSvc: Title) {
      titleSvc.setTitle('Home')
  }

}
