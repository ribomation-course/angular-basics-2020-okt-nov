import {Route} from '@angular/router';
import {WelcomeComponent} from './pages/welcome/welcome.component';
import {AboutComponent} from './pages/about/about.component';
import {UsersComponent} from './pages/users/users.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';

export const routingTable: Route[] = [
    {path: 'welcome', component: WelcomeComponent, data: {label: 'Home', icon: 'fas fa-home'}},
    {path: 'about', component: AboutComponent},
    {path: 'users', component: UsersComponent},
    {path: '', redirectTo: '/welcome', pathMatch: 'full'},
    {path: '**', component: NotFoundComponent},
];

