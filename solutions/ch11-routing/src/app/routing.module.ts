import {NgModule} from '@angular/core';
import {routingTable} from './routes';
import {RouterModule} from '@angular/router';


@NgModule({
    imports: [
        RouterModule.forRoot(routingTable)
    ],
    exports: [
        RouterModule
    ]
})
export class RoutingModule {
}
