import {Component} from '@angular/core';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {filter, map} from 'rxjs/operators';

interface Page {
    uri: string;
    label: string;
    icon?: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    pages: Page[] = [
        {uri: '/welcome', label: 'Home'},
        {uri: '/users', label: 'User List'},
        {uri: '/about', label: 'About'},
    ];

    constructor(private router: Router) {
        router.events
            .pipe(
                filter((ev:RouterEvent) => ev instanceof NavigationEnd),
                map((ev:NavigationEnd) => ev.urlAfterRedirects)
            )
            .subscribe(url => {
                console.log('url: %o', url);
            })
    }
}
