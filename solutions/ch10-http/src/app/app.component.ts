import {Component, OnInit} from '@angular/core';
import {User, UserService} from './services/user.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    users: User[] = [];
    user: User;
    newUser: User;

    constructor(public userSvc: UserService) {
    }

    ngOnInit(): void {
        this.reset();
        this.update();
    }

    private update() {
        this.userSvc.findAll()
            .subscribe(objs => this.users = objs);
    }

    findById(id: number): void {
        this.userSvc.findById(id)
            .subscribe(obj => this.user = obj);
    }

    edit(u: User): void {
        this.newUser = Object.assign({}, u);
    }

    save() {
        const payload: User = Object.assign({}, this.newUser);
        if (payload.id) {
            this.userSvc.update(payload)
                .subscribe((obj: User) => {
                    //this.update()
                    const existingUser = this.users.find(u => u.id === obj.id);
                    existingUser.name = obj.name;
                    existingUser.age = obj.age;
                });
        } else {
            this.userSvc.create(payload)
                .subscribe((obj: User) => {
                    //this.update()
                    this.users.push(obj);
                }, err => {
                    console.error('failed: %o', err);
                });
        }
        this.reset();
    }

    delete(id: number): void {
        this.userSvc.remove(id)
            .subscribe(_ => {
                //this.update();
                const idx = this.users.findIndex(u => u.id === id);
                this.users.splice(idx, 1);
            });
    }

    reset() {
        this.newUser = {name: '', age: 18};
    }

}
