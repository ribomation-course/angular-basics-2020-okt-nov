import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface User {
    id?: number;
    name: string;
    age: number;
}

function URL(id?: number): string {
    const url = 'http://localhost:3000/users';
    if (id && +id > 0) {
        return `${url}/${id}`;
    }
    return url;
}

@Injectable({providedIn: 'root'})
export class UserService {
    constructor(private http: HttpClient) {}

    findAll(): Observable<User[]> {
        return this.http.get<User[]>(URL());
    }

    create(user: User): Observable<User> {
        return this.http.post<User>(URL(), user);
    }

    findById(id: number): Observable<User> {
        return this.http.get<User>(URL(id));
    }

    update(user: User): Observable<User> {
        return this.http.put<User>(URL(user.id), user);
    }

    remove(id: number): Observable<void> {
        return this.http.delete<void>(URL(id));
    }

}
