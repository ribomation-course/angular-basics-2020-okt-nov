import {Component} from '@angular/core';
import {User} from './user.domain';

type MessageType = 'submit' | 'cancel';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    user: User = {name: 'Per', age: 0};
    message: string;
    messageType: MessageType;


    onSubmit(payload: User) {
        this.showMessage('User submitted: ' + JSON.stringify(payload), 'submit');
        this.user = payload;
    }

    onCancel() {
        this.showMessage('User cancelled', 'cancel');
    }

    showMessage(txt: string, type: MessageType) {
        this.message = txt;
        this.messageType = type;
        setTimeout(() => {
            this.message = undefined;
        }, 4000);
    }

}
