import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../user.domain';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'component-oriented-form',
    templateUrl: './component-oriented-form.component.html',
    styleUrls: ['../form-styles.scss']
})
export class ComponentOrientedFormComponent implements OnInit {
    @Input('user') user: User;
    @Output('save') submitEmitter = new EventEmitter<User>();
    @Output('keep') cancelEmitter = new EventEmitter<void>();
    form = new FormGroup({});
    nameCtrl = new FormControl('', [Validators.required, Validators.minLength(5)]);
    ageCtrl = new FormControl('', [Validators.required, Validators.min(25), Validators.max(65)]);

    ngOnInit(): void {
        if (!this.user) {
            throw new Error('[ComponentOrientedForm] must provide user object');
        }

        this.nameCtrl.setValue(this.user.name);
        this.ageCtrl.setValue(this.user.age);
        this.form.addControl('name', this.nameCtrl);
        this.form.addControl('age', this.ageCtrl);
    }

    onSubmit() {
        if (this.form.valid) {
            this.submitEmitter.emit(Object.assign({}, this.form.value));
            this.form.reset({name:'',age:0});
        }
    }

    onCancel() {
        this.form.reset();
        this.cancelEmitter.emit();
    }

}
