import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {User} from '../../user.domain';
import {FormGroup} from '@angular/forms';

@Component({
    selector: 'template-oriented-form',
    templateUrl: './template-oriented-form.component.html',
    styleUrls: ['../form-styles.scss']
})
export class TemplateOrientedFormComponent implements OnInit {
    @Input('user') user: User;
    @Output('submit') submitEmitter = new EventEmitter<User>();
    @Output('cancel') cancelEmitter = new EventEmitter<void>();
    @ViewChild('form') form: FormGroup;

    ngOnInit(): void {
        if (!this.user) {
            throw new Error('[TemplateOrientedForm] must provide user object');
        }
    }

    onSubmit() {
        if (this.form.valid) {
            this.submitEmitter.emit(Object.assign({}, this.user));
            this.form.reset();
        }
    }

    onCancel() {
        this.form.reset();
        this.cancelEmitter.emit();
    }

}
