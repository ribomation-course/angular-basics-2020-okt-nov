import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { WelcomePage } from './pages/welcome/welcome.page';
import { ProductsPage } from './pages/products/products.page';
import { ShoppingBasketPage } from './pages/shopping-basket/shopping-basket.page';
import { NavbarWidget } from './widgets/navbar/navbar.widget';
import { BasketWidget } from './widgets/basket/basket.widget';
import {ProductsService} from "./services/products.service";
import {FakeProductsService} from "./services/FakeProductsService";
import {LoggerService} from "./services/logger.service";

import {environment} from '../environments/environment'

const LoggerServiceFactory = () => {
    return new LoggerService(environment.production === false)
}

@NgModule({
    declarations: [
        AppComponent,
        WelcomePage,
        ProductsPage,
        ShoppingBasketPage,
        NavbarWidget,
        BasketWidget
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
    ],
    providers: [
        {provide: ProductsService, useClass: FakeProductsService},
        {provide: LoggerService, useFactory: LoggerServiceFactory}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
