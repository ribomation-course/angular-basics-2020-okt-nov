import {Injectable} from '@angular/core';

@Injectable()
export class LoggerService {

    constructor(private verbose: boolean ) {
    }

    debug(msg: string): void {
        if (this.verbose) {
            console.log('[LOG] %s: %s', new Date().toLocaleTimeString(), msg)
        }
    }

}


