import {ProductsService} from "./products.service";
import {Observable, of} from "rxjs";
import {Product} from "../domain/product.domain";

export class FakeProductsService extends ProductsService {
    readonly fakeData: Product[] = [
        {name: 'Apple', price: 123, id: 'abc', image: '/assets/defaultimg.png'},
        {name: 'Banana', price: 456, id: 'qwerty', image: '/assets/defaultimg.png'},
    ];

    findAll(): Observable<Product[]> {
        return of(this.fakeData)
    }

}
