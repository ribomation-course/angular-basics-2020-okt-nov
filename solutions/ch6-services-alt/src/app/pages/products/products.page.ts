import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Product} from "../../domain/product.domain";
import {ProductsService} from "../../services/products.service";
import {BasketService} from "../../services/basket.service";
import {LoggerService} from "../../services/logger.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss']
})
export class ProductsPage implements OnDestroy{
    products$: Observable<Product[]>;

    constructor(
        public productsSvc: ProductsService,
        public basketSvc: BasketService,
        private logger:LoggerService
    ) {
        this.products$ = productsSvc.findAll();
        //console.log('ProductsPage ENTER')
        logger.debug('ProductsPage ENTER')
    }

    ngOnDestroy(): void {
        //console.log('ProductsPage EXIT')
        this.logger.debug('ProductsPage EXIT')
    }


}
