import {Component} from '@angular/core';
import {LoggerService} from "../../services/logger.service";

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.page.html',
    styleUrls: ['./welcome.page.scss']
})
export class WelcomePage {
    image = 'http://localhost:3000/img/store.jpg'

    constructor(private logger: LoggerService) {
    }

    showDefault() {
        this.image = '/assets/defaultimg.png'
        //console.log('swapped image to default')
        this.logger.debug('swapped image to default')
    }
}
