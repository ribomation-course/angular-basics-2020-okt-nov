import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    fullName: string = 'Anna Conda';
    address: string = '42 Hacker Street';
    phone: string = '08-123456';

    onPropertyUpdate(ev) {
        console.log('on-property-update: %o', ev)
    }
}
