import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'editable-text',
    templateUrl: './editable-text.widget.html',
    styleUrls: ['./editable-text.widget.css']
})
export class EditableTextWidget implements OnInit {
    @Input('value') origValue: string;
    @Input('empty') hint: string = 'Click to Edit';
    @Output('update') saveEmitter = new EventEmitter<any>();
    @Input('property') property:string = 'no-name';

    labelVisible: boolean = true;
    formValue:string;

    constructor() {
    }

    ngOnInit(): void {
    }

    showForm() {
        this.formValue = this.origValue;
        this.labelVisible = false;
    }

    cancel() {
        this.labelVisible = true;
    }

    save(newValue: string) {
        this.origValue = newValue;
        this.labelVisible = true;
        this.saveEmitter.emit({
            property: this.property,
            value: this.origValue
        })
    }

    onKeyEvent(ev: KeyboardEvent, newValue: string) {
        if (ev.key === 'Escape') {
            this.cancel();
        } else if (ev.key === 'Enter') {
            this.save(newValue);
        }
    }

}
