"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Account = void 0;
class Account {
    constructor(_accno, _balance) {
        this._accno = _accno;
        this._balance = _balance;
    }
    get accno() {
        return this._accno.toUpperCase();
    }
    get balance() {
        return this._balance;
    }
    set balance(amount) {
        this._balance += amount;
    }
    toString() {
        return `Account[${this.accno}, ${this._balance} kr]`;
    }
}
exports.Account = Account;
// let a: Account = new Account('seb1234', 125)
// console.log('a: %s', a.toString());
// a.balance = 50
// console.log('a: %s', a.toString());
