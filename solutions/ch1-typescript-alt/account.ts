export class Account {
    constructor(
        private _accno: string,
        private _balance: number
    ) { }

    get accno(): string {
        return this._accno.toUpperCase()
    }

    get balance(): number {
        return this._balance;
    }

    set balance(amount: number) {
        this._balance += amount
    }

    toString() {
        return `Account[${this.accno}, ${this._balance} kr]`
    }
}

// let a: Account = new Account('seb1234', 125)
// console.log('a: %s', a.toString());

// a.balance = 50
// console.log('a: %s', a.toString());

