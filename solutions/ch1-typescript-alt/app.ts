import { Account } from './account'
import { Generator } from './generator'


let gen = new Generator()
let accounts: Account[] = []
const N = 100

for (let k = 1; k <= N; ++k) accounts.push(gen.nextAccount())

accounts.forEach(a => console.log('** %s', a.toString()))
