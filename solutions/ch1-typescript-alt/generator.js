"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Generator = void 0;
const account_1 = require("./account");
class Generator {
    constructor() {
        this.bankNames = [
            'SEB', 'HB', 'SWE'
        ];
    }
    nextInt(lb, ub) {
        return Math.floor(Math.random() * (ub - lb + 1) + lb);
    }
    nextAccount() {
        let accno = this.bankNames[this.nextInt(0, this.bankNames.length - 1)] + this.nextInt(1000, 9999);
        let balance = this.nextInt(100, 999);
        return new account_1.Account(accno, balance);
    }
}
exports.Generator = Generator;
// let g=new Generator()
// let a = g.nextAccount()
// console.log(a.toString());
