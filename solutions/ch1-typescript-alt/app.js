"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const generator_1 = require("./generator");
let gen = new generator_1.Generator();
let accounts = [];
const N = 10;
for (let k = 1; k <= N; ++k)
    accounts.push(gen.nextAccount());
accounts.forEach(a => console.log('** %s', a.toString()));
