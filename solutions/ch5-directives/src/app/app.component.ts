import {Component} from '@angular/core';

export interface Person {
  first_name: string;
  last_name: string;
  email: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  readonly PERSONS: Person[] = [
    {'first_name': 'Sherwynd', 'last_name': 'Hale', 'email': 'shale0@loc.gov'},
    {'first_name': 'Esta', 'last_name': 'Giacopello', 'email': 'egiacopello1@cdc.gov'},
    {'first_name': 'Tiebout', 'last_name': 'Voice', 'email': 'tvoice2@mayoclinic.com'},
    {'first_name': 'Bunni', 'last_name': 'Reck', 'email': 'breck3@goo.gl'},
    {'first_name': 'Grant', 'last_name': 'Corbally', 'email': 'gcorbally4@hubpages.com'},
    {'first_name': 'Minnaminnie', 'last_name': 'Imorts', 'email': 'mimorts5@ed.gov'},
    {'first_name': 'Haily', 'last_name': 'Tease', 'email': 'htease6@parallels.com'},
    {'first_name': 'Shanda', 'last_name': 'Stealey', 'email': 'sstealey7@army.mil'},
    {'first_name': 'Jose', 'last_name': 'Darter', 'email': 'jdarter8@chronoengine.com'},
    {'first_name': 'Noam', 'last_name': 'Terzi', 'email': 'nterzi9@dell.com'}
  ];
  persons: Person[] = this.PERSONS;

  toggle() {
    if (this.persons.length > 0) {
      this.persons = [];
    } else {
      this.persons = this.PERSONS;
    }
  }

}
