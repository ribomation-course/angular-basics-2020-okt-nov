export class Product {
    constructor(
        public name: string,
        public price: number,
        public itemsInStore: number,
        public isService: boolean,
        public lastUpdate: Date
    ) {
    }

    static create() {
        const name = Product.names[Product.int(0, Product.names.length - 1)];
        const price = Product.int(500, 5000);
        const count = Math.random() * 10;
        const svc = Math.random() < .5;
        const date = new Date(Date.now() - Product.int(0, 30) * 24 * 3600 * 1000);
        return new Product(name, price, count, svc, date);
    }

    private static names: string[] = [
        'apple', 'banana', 'coco nut', 'date plum'
    ];

    private static int(lb: number, ub: number): number {
        return Math.floor(Math.random() * (ub - lb + 1) + lb);
    }

}
