import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'kronor'
})
export class KrPipe implements PipeTransform {
    transform(value: number, oren: boolean = false): string {
        if (!value) {
            return '0 kr';
        }

        const opts = {
            useGrouping: true,
            minimumFractionDigits: oren ? 2 : 0,
            maximumFractionDigits: oren ? 2 : 0,
        };
        return <string> (Number(value).toLocaleString('sv-SE', opts) + ' kr')
            .replace(/\s+/g, '\xA0');
    }
}
