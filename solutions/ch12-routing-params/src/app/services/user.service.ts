import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface User {
  id: string;
  name: string;
  title: string;
  company: string;
  city: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly baseUrl: string = 'http://localhost:3000/users';

  //private _cachedUser: User;

  constructor(private http: HttpClient) {
  }

  findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
  }

  findById(id: string): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/${id}`);
  }

  remove(id: string): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${id}`);
  }

  create(user: any): Observable<User> {
    return this.http.post<User>(this.baseUrl, user);
  }

  get cached(): User {
    const json = sessionStorage.getItem('cached.user');
    return JSON.parse(json);
  }

  set cached(usr: User) {
    sessionStorage.setItem('cached.user', JSON.stringify(usr));
  }

}
