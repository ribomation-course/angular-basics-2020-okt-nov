import {Component, OnInit} from '@angular/core';
import {User, UserService} from '../../services/user.service';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users$: Observable<User[]>;

  constructor(
    public userSvc: UserService,
    private router: Router
  ) {
    this.users$ = userSvc.findAll();
  }

  ngOnInit(): void {
  }

  gotoUser(usr: User) {
    console.log('goto: %o', usr);
    this.userSvc.cached = usr;
    this.router.navigate(['/user', usr.id]);
  }

}
