import {Component, OnInit} from '@angular/core';
import {User, UserService} from '../../services/user.service';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';
import {Title} from '@angular/platform-browser';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
    user: User;

    constructor(
        private route: ActivatedRoute,
        private userSvc: UserService,
        private titleSvc: Title
    ) {
    }

    ngOnInit(): void {
        const cachedUser = this.userSvc.cached;

        if (cachedUser) {
            this.user = cachedUser;
            this.titleSvc.setTitle(`User: ${cachedUser.name}`);
            console.log('using cached version: %o', cachedUser);
        } else {
            const id = this.route.snapshot.paramMap.get('id');
            console.log('fetching uid=%s from back-end', id);
            this.userSvc.findById(id).subscribe(usr => {
                this.user = usr;
                this.titleSvc.setTitle(`User: ${usr.name}`);
            });
        }
    }

}
