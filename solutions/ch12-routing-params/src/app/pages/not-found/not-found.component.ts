import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <h1>Not Found</h1>
    <p>
      Ooops, cannot find this page
    </p>
  `,
  styles: [
    'p {color: red;}'
  ]
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
