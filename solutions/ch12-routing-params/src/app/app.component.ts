import {Component} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Route, Router} from '@angular/router';
import {filter, map, mergeMap} from 'rxjs/operators';
import {routes} from './routes';
import {Title} from '@angular/platform-browser';

interface Page {
    uri: string;
    label: string;
    icon?: string;
}

interface Nav {
    title: string;
    navbar: number;
}

interface RouteNav extends Route {
    data: Nav;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    pages: Page[] = (routes as RouteNav[])
        .filter(r => !!r.data)
        .filter(r => r.data.navbar && r.data.navbar > 0)
        .sort((lhs, rhs) => lhs.data.navbar - rhs.data.navbar)
        .map(r => ({uri: '/' + r.path, label: r.data.title} as Page))
    ;
    // [
    //     {uri: '/welcome', label: 'Home'},
    //     {uri: '/users', label: 'User List'},
    //     {uri: '/about', label: 'About'},
    // ];

    constructor(
        private router: Router,
        private currentRoute: ActivatedRoute,
        private titleSvc: Title
    ) {

        // https://stackoverflow.com/questions/43512695/how-to-get-route-data-into-app-component-in-angular-2
        // https://ultimatecourses.com/blog/dynamic-page-titles-angular-2-router-events
        router.events
            .pipe(
                filter(ev => ev instanceof NavigationEnd),
                map(_ => this.currentRoute),
                map(route => {
                    while (route.firstChild) {
                        route = route.firstChild;
                    }
                    return route;
                }),
                filter(route => route.outlet === 'primary'),
                mergeMap(route => route.data)
            )
            .subscribe((nav: Nav) => {
                console.log('nav: %o', nav);
                this.titleSvc.setTitle(`${nav.title} :: ng Routing Demo`);
            });

    }

}
