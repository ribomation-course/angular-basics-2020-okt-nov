import {Route} from '@angular/router';
import {WelcomeComponent} from './pages/welcome/welcome.component';
import {AboutComponent} from './pages/about/about.component';
import {UsersComponent} from './pages/users/users.component';
import {UserComponent} from './pages/user/user.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';

export const routes: Route[] = [
    {
        path: 'welcome', component: WelcomeComponent, data: {
            title: 'Home', navbar: 1
        }
    },
    {
        path: 'about', component: AboutComponent, data: {
            title: 'About Us', navbar: 3
        }
    },
    {
        path: 'users', component: UsersComponent, data: {
            title: 'User List', navbar: 2
        }
    },
    {
        path: 'user/:id', component: UserComponent, data: {
            title: 'User Detail',
        }
    },
    {path: '', redirectTo: '/welcome', pathMatch: 'full'},
    {path: '**', component: NotFoundComponent}
];
