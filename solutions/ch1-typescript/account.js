"use strict";
exports.__esModule = true;
exports.Account = void 0;
var Account = /** @class */ (function () {
    function Account(accno, balance, credit) {
        this.accno = accno;
        this.balance = balance;
        this.credit = credit;
    }
    Account.prototype.toString = function () {
        return "Account[" + this.accno + ", " + this.balance + " kr, has " + (this.credit ? '' : 'no ') + "credit]";
    };
    return Account;
}());
exports.Account = Account;
