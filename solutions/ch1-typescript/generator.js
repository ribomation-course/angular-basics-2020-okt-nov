"use strict";
exports.__esModule = true;
exports.Generator = void 0;
var account_1 = require("./account");
var Generator = /** @class */ (function () {
    function Generator() {
        this.bankNames = [
            'SEB', 'HB', 'SWE'
        ];
    }
    Generator.prototype.nextInt = function (lb, ub) {
        return Math.floor(Math.random() * (ub - lb + 1) + lb);
    };
    Generator.prototype.nextAccount = function () {
        var accno = this.bankNames[this.nextInt(0, this.bankNames.length - 1)] + this.nextInt(1000, 9999);
        var balance = this.nextInt(100, 999);
        var credit = (Math.random() < .75);
        return new account_1.Account(accno, balance, credit);
    };
    return Generator;
}());
exports.Generator = Generator;
