import { Account } from './account'
import { Generator } from './generator'

const gen = new Generator()
const list: Account[] = []
const N = 10

for (let k = 1; k <= N; ++k) {
    list.push(gen.nextAccount())
}

list.forEach(acc => console.log(acc.toString()))
