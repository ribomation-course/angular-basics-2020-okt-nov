
export class Account {
    constructor(
        private accno: string,
        private balance: number,
        private credit: boolean,
    ) { }

    toString() {
        return `Account[${this.accno}, ${this.balance} kr, has ${this.credit ? '' : 'no '}credit]`
    }
}
