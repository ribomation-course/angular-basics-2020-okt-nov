import { Account } from './account'

export class Generator {
    private readonly bankNames: string[] = [
        'SEB', 'HB', 'SWE'
    ]

    private nextInt(lb: number, ub: number): number {
        return Math.floor(Math.random() * (ub - lb + 1) + lb)
    }

    nextAccount() {
        const accno = this.bankNames[this.nextInt(0, this.bankNames.length - 1)] + this.nextInt(1000, 9999)
        const balance = this.nextInt(100, 999)
        const credit = (Math.random() < .75)
        return new Account(accno, balance, credit)
    }
}

