import {Injectable} from '@angular/core';
import {Product} from "../product.domain";

export class Item {
  private _count: number;

  constructor(private _id: number, private _product: Product) {
    this._count = 1;
  }

  get id(): number {
    return this._id;
  }

  get name(): string {
    return this._product.name;
  }

  get image(): string {
    return this._product.url;
  }

  get price(): number {
    return this._product.price;
  }

  get count(): number {
    return this._count;
  }

  get total(): number {
    return this.count * this.price;
  }

  add() {
    this._count++;
  }
}

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  private _items: Item[] = [];

  add(id: number, product: Product) {
    const found = this._items.find(item => item.id === id);
    if (found) {
      found.add();
    } else {
      this._items.push(new Item(id, product));
    }
  }

  clear() {
    this._items = [];
  }

  get items(): Item[] {
    return this._items;
  }

  get empty(): boolean {
    return this.items.length === 0;
  }

  get total(): number {
    return this.items
      .map(item => item.total)
      .reduce((sum, cost) => sum + cost, 0);
  }
}

