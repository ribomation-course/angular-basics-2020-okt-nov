import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../services/products.service";
import {ShoppingCartService} from "../services/shopping-cart.service";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {

  constructor(
    public products: ProductsService,
    public cart: ShoppingCartService
  ) {
  }


}
