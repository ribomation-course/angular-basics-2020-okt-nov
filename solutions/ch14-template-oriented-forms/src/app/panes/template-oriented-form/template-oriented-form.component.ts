import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {User} from '../../user.domain';
import {FormGroup} from '@angular/forms';

@Component({
    selector: 'template-oriented-form',
    templateUrl: './template-oriented-form.component.html',
    styleUrls: ['./template-oriented-form.component.scss']
})
export class TemplateOrientedFormComponent implements OnInit {
    @Input('user') user: User;
    @Output('submit') submitEmitter = new EventEmitter<User>();
    @Output('cancel') cancelEmitter = new EventEmitter<void>();
    @ViewChild('form') form: FormGroup;

    constructor() {
    }

    ngOnInit(): void {
        if (!this.user) {
            throw new Error('[TemplateOrientedForm] must provide user object');
        }
    }

    onSubmit() {
        if (this.form.valid) {
            let payload = Object.assign({}, this.user);
            console.log('*** %o', payload);

            this.submitEmitter.emit(payload);
        }
    }

    onCancel() {
        this.cancelEmitter.emit();
    }

}
