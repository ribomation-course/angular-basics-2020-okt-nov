export interface User {
    name: string;
    age?: number;
    female?: boolean;
    nextBirthDay?: Date;
}
