import {Component} from '@angular/core';
import {MultiplyService} from './services/multiply.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  value:number = 5;

  constructor(
    public svc: MultiplyService
  ) {
  }

  updateFactor(ev: any) {
    console.log('update: %o', ev);
    const val = Number(ev.target.value);
    this.svc.setFactor(val);
  }
}
