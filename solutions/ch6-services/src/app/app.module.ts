import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {MultiplyService} from './services/multiply.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    {provide: MultiplyService, useClass: MultiplyService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
