import {Injectable} from '@angular/core';
//{providedIn: 'root'}
@Injectable()
export class MultiplyService {
  private factor: number = 10;

  mul(arg: number): number {
    return arg * this.factor;
  }

  setFactor(val:number):void {
    this.factor = val;
  }

}
