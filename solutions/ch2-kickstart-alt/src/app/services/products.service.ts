import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "../domain/product.domain";
import {Observable} from "rxjs";
import {map, tap} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class ProductsService {
    private readonly baseUrl = 'http://localhost:3000'
    private readonly productsUrl = this.baseUrl + '/products';

    constructor(private http: HttpClient) {
    }

    findAll(): Observable<Product[]> {
        return this.http
            .get<Product[]>(this.productsUrl)
            .pipe(
                map(list =>
                    list.map(p => {
                            p.image = this.baseUrl + p.image;
                            return p;
                        }
                    )
                ),
                tap(payload => console.log('*** %o', payload))
            );
    }

}
