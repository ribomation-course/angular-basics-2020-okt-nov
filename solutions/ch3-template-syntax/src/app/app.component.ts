import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    safe = false;

    user: any = {
        name: {
            first: 'Anna',
            last: 'Conda'
        }
    };

    toggle() {
        if (this.user) {
            this.user = undefined;
        } else {
            this.user = {
                name: {
                    first: 'Inge',
                    last: 'Vidare'
                }
            };
        }
    }

}
