# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.

* Zoom Client
  - https://us02web.zoom.us/download
  - [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* GIT Client
  - https://git-scm.com/downloads
* A BASH terminal, such as the GIT client terminal
* NodeJS / NPM
  - https://nodejs.org/en/download/
* Angular CLI
  - `npm install -g @angular/cli`
* A decent IDE, such as
    * MicroSoft Visual Code
        - https://code.visualstudio.com/
    * JetBrains WebStorm
        - https://www.jetbrains.com/webstorm/download
* A modern browser, such as any of
  - [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)
  - [Mozilla FireFox](https://www.mozilla.org/sv-SE/firefox/new/)
  - [MicroSoft Edge](https://www.microsoft.com/sv-se/windows/microsoft-edge)

# Speciella instructioner för Trafikverket
*Följande instruktioner har jag fått från en deltagare och återger dem här verbatimt.*

## Installation av Node.js
Installation av `Node.msi` stoppas direkt med meddelandet 

"*Systemadministratören har angett systemprinciper som hindrar den här installationen*".

Lösningen är att köra filen från `C:\Windows\ccmcache`

## Installation av Angular CLI

Kommandot `npm install -g @angular/cli` fungerar inte alls eftersom TRV stängt möjligheten att hämta publika paket.

Lösningen är att först köra dessa 3 kommandon från en command prompt:

    npm config set registry=http://proget.trafikverket.local:81/npm/DefaultNpm/
    npm config set strict-ssl=false
    npm config set sass-binary-site=http://proget:1337/node-sass



